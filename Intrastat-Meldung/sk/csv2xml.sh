#!/bin/bash
# ©2021, J. Schwender
# ABAS Version 2017 erzeugt im Intrastat-Meldezentrum ausschliesslich ASCII Meldedateien, diese sind ab 2022 nicht mehr zulässig
# Das Meldezentrum wird gepatcht, so, dass eine zusätzliche CSV Meldedatei erzeugt wird. Diese könnte vorerst zur Meldung verwendet werden,
# das eigentlich bevorzugte Format ist aber XML. Zum 13 Dezember 2021 ist eine Schnittstellenbeschreibung  vom Stat.Bundesamt veröffentlicht worden.
# Dieses Skript wandelt die erzeugte CSV Datei in eine XML Datei um.
# Die ABAS-Einbindung erfolgt mit dem Patch von 2 Dateien: /sk/INTRASTATZENTRALE.[BN|MELD]
#
ZEITSTEMPEL=$(date -Is)
        DATUMVOLL=$(date --date=$ZEITSTEMPEL +%Y%m-%M:%H:%S)
         DATUMISO=$(date --date=$ZEITSTEMPEL -I)
          UHRZEIT=$(date --date=$ZEITSTEMPEL +%X)

   MATERIALNUMMER='XGCMP'                     # wurde vom Statistischen Bundesamt am 2022-01-06 nach erfolgreichem Test vergeben 
             PSID='0000000000000000'          # PartyID, 16 stelliger Melder-Identifikator = SchlüsselBundesLand + Steuernummer(UStVA)+"0"+Unterschiedungsnummer

            INPUT="DAT26X.CSV"                # wird so immer vom (gepatchten) Meldezentrum in der ABAS-Version 2017 erzeugt
              AUS="DAT26.XML"                 # Seit 2022 ist XML zur Übermittlung angesagt, ASCII wird stillgelegt

if [ -n "$1" ]
then
    INPUT=${DIR_MANDANTDIR}/$1                # Voreinstellung wird von Kommandozeilenoption überschrieben
fi
function write_header() {
        ZEILE=$(head -1 "$INPUT")             # eine Zeile aus der CSV-Datei
       BEZMON=$(echo $ZEILE|cut -d';' -f2)    # 
      BEZJAHR=20$(echo $ZEILE|cut -d';' -f18)
  MELDENUMMER=$(echo $ZEILE|cut -d';' -f19)
          AUS=${DIR_MANDANTDIR}/win/tmp/${MATERIALNUMMER}-${BEZJAHR}${BEZMON}-$(date --date=$ZEITSTEMPEL +%Y%m%d-%H%M).xml     # hier soll das Ergebnis hin

echo "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>
<INSTAT>
  <Envelope>
    <envelopeId>${MATERIALNUMMER}-20${BEZJAHR}${BEZMON}-$DATUMVOLL</envelopeId>
    <DateTime><date>$DATUMISO</date><time>$UHRZEIT</time></DateTime>
    <Party partyType=\"CC\" partyRole=\"receiver\">
      <partyId>00</partyId>
      <partyName>Statistisches Bundesamt</partyName>
      <Address>
        <streetName>Gustav - Stresemann - Ring 11</streetName>
        <postalCode>65189</postalCode>
        <cityName>Wiesbaden</cityName>
      </Address>
    </Party>
    <Party partyType=\"PSI\" partyRole=\"sender\">
      <partyId>${PSID}</partyId>
      <partyName>Musterfirma GmbH</partyName>
      <interchangeAgreementId>XGTEST</interchangeAgreementId>
      <Address>
        <streetName>Musterstrasse 1</streetName>
        <postalCode>01234</postalCode>
        <cityName>Musterstadt</cityName>
        <countryName>Deutschland</countryName>
        <phoneNumber>+49 030 111 0 111</phoneNumber>
        <e-mail>info@firmendomaene.de</e-mail>
        <URL>www.firmendomaene.de</URL>
      </Address>
    </Party>
    <testIndicator>true</testIndicator>
    <softwareUsed>ABAS Intrastat Report Infosystem with subsequent conversion 1.0</softwareUsed>" > "$AUS"
}

function write_item() {
WARENNUMMER=$(echo $1|cut -d';' -f10)
VERKEHRSZWEIG=$(echo $1|cut -d';' -f4)   # modeOfTransportCode
BESTIMMUNGSLAND=$(echo $1|cut -d';' -f6)
URSPRUNGSREGION=$(echo $1|cut -d';' -f8)
MASSE=$(echo $1|cut -d';' -f12)
BESME=$(echo $1|cut -d';' -f13)
REBETR=$(echo $1|cut -d';' -f14)
STBETR=$(echo $1|cut -d';' -f15)
TACODEA=$(echo $1|cut -d';' -f3|cut -c 1)  # Transaktionscode A
TACODEB=$(echo $1|cut -d';' -f3|cut -c 2)
PARTNERID=$(echo $1|cut -d';' -f16)
INVOICENO=$(echo $1|cut -d';' -f17)

echo "      <Item>
        <itemNumber>${ZEILENNUMMER}</itemNumber>
        <CN8>
    	    <CN8Code>${WARENNUMMER}</CN8Code>
    	    <SUCode></SUCode>
        </CN8>
        <goodsDescription>nicht verfuegbar</goodsDescription>
        <MSConsDestCode>${BESTIMMUNGSLAND}</MSConsDestCode>
        <countryOfOriginCode>${URSPRUNGSREGION}</countryOfOriginCode>
        <netMass>${MASSE}</netMass>
        <quantityInSU>${BESME}</quantityInSU>
        <invoicedAmount>${REBETR}</invoicedAmount>
        <statisticalValue>${STBETR}</statisticalValue>
        <NatureOfTransaction>
    	    <natureOfTransactionACode>${TACODEA}</natureOfTransactionACode>
    	    <natureOfTransactionBCode>${TACODEB}</natureOfTransactionBCode>
        </NatureOfTransaction>
        <modeOfTransportCode>$VERKEHRSZWEIG</modeOfTransportCode>
        <regionCode>99</regionCode>
	<invoiceNumber>$INVOICENO</invoiceNumber>
        <partnerId>$PARTNERID</partnerId>
      </Item>" >> "$AUS"
}



function write_declaration_start() {
echo "    <Declaration>
      <declarationId>${MELDENUMMER}</declarationId>
      <DateTime>
        <date>${DATUMISO}</date>
        <time>${UHRZEIT}</time>
      </DateTime>
      <referencePeriod>${BEZJAHR}-${BEZMON}</referencePeriod>
      <PSIId>${PSID}</PSIId>
      <Function>
         <functionCode>O</functionCode>
         <previousDeclarationId/>
      </Function>
      <declarationTypeCode/>
      <flowCode>D</flowCode>
      <currencyCode>2</currencyCode>" >> "$AUS"
}

function write_declaration_end() {
echo "    </Declaration>" >> "$AUS"
}

function write_end() {
echo "  </Envelope>
</INSTAT>" >> "$AUS"
}
#######################################################################
write_header
write_declaration_start
ZEILENNUMMER=1
while read ZEILE 
do
    write_item $ZEILE
    ((ZEILENNUMMER++))
done < $INPUT
write_declaration_end
write_end

