#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
    ©2021, Joachim Schwender, alle Rechte vorbehalten
    Programm zur Konvertierung von ASCII-Dateien in XML Dateien,
    Angepasst für die speziellen Erfordenisse der ABAS-Intrastat-Meldungsdatei DAT26.ASC
    In der ABAS-Version 2017 gibt es dafür keine offizielle Anpassungsmöglichkeit mit dem Verweis auf ein Versions-Update.
    Dies hier ist ein Notbehelf um in dieser Version eine Anpassung an die seit 2021 zwingend geforderte XML Übertragung
    zu bewerkstelligen.
    
    Die Eingangsdatei ist eine aus ABAS exportierte ASCII Datei mit fixen Feldlängen. Diese wird geparst, die Daten extrahiert und 
    in dem vorgegebenen XML Format ausgegeben. Dies ist dann nur der Teil mit den Items: <Item>… …</Item>, der 
    Header und Envelope müssen noch ergänzt werden.
    
    Dieses Programm ist funktionsfähig, dennoch wurde es zugunsten einer reinen bash-Variante nicht eingesetzt.
    Der Grund ist, dass die hohe Verarbeitungsgeschwindigkeit von C kein effektiver Vorteil ist,
    da selbst in bash die Laufzeit sehr klein ist. Möglicher Weise käme dieser Vorteil zum Tragen wenn die Anzahl der Items sehr gross wird.
*/
int main (int argc, char *argv[]) {
    FILE *datei;
    int i;
    char Zeile[124];
    char Bestimmungsland[3];
    char Ursprungsregion[3];
    char ArtDesGesch[3];
    char WTN[9];
    char BezugsMonat[3];
    char BezugsJahr[3];
    char TempStr[14];  // um in numerische Werte zu wandeln und dabei führende Nullen zu eliminieren
    int PositionsNr;
    int  Eigenmasse;
    int  BesEinheit;
    int  ReBetrag;
    int  StatWert;

//    datei = fopen("DAT26.ASC","r");   // Datei wird geöffnet ()
    datei = fopen(argv[1],"r");   // Datei wird geöffnet ()
    if (argc<=1) {
    printf("Bitte Dateinamen angeben!\n");
    return EXIT_FAILURE;       // Programm wird beendet, wenn Datei nicht geöffent werden konnte
    }

    if(NULL == datei) {
    printf("Datei konnte nicht geöffnet werden!\n");
    return EXIT_FAILURE;       // Programm wird beendet, wenn Datei nicht geöffent werden konnte
    }

   /* Inhalt der Textdatei zeilenweise ausgeben*/

   while(!feof(datei)) {
    fgets(Zeile,123,datei);
    Bestimmungsland[0]=Zeile[32];  // rechtsbündig ISO-alpha-2 code in 3stelligem code-feld
    Bestimmungsland[1]=Zeile[33];
    Bestimmungsland[2]='\0';

    Ursprungsregion[0]=Zeile[34];
    Ursprungsregion[1]=Zeile[35];
    Ursprungsregion[2]='\0';

    ArtDesGesch[0]=Zeile[36];
    ArtDesGesch[1]=Zeile[37];
    ArtDesGesch[2]='\0';
    for (i=0;i<8;i++) { WTN[i]=Zeile[52+i]; }; WTN[8]='\0';

    for (i=0;i<7;i++) { TempStr[i]=Zeile[7+i]; }; TempStr[7]='\0';
    sscanf(TempStr,"%d", &PositionsNr);                    // führende Nullen loswerden

    for (i=0;i<12;i++) { TempStr[i]=Zeile[70+i]; }; TempStr[12]='\0';
    sscanf(TempStr,"%d", &Eigenmasse);                    // führende Nullen loswerden

    for (i=0;i<12;i++) { TempStr[i]=Zeile[81+i]; }; TempStr[12]='\0';
    sscanf(TempStr, "%d", &BesEinheit);

    for (i=0;i<12;i++) { TempStr[i]=Zeile[94+i]; }; TempStr[12]='\0';
    sscanf(TempStr, "%d", &ReBetrag);

    for (i=0;i<12;i++) { TempStr[i]=Zeile[105+i]; }; TempStr[12]='\0';
    sscanf(TempStr, "%d", &StatWert);

    BezugsMonat[0]=Zeile[118];
    BezugsMonat[1]=Zeile[119];
    BezugsMonat[2]='\0';

    BezugsJahr[0]=Zeile[120];
    BezugsJahr[1]=Zeile[121];
    BezugsJahr[2]='\0';

    printf("	<item>\n");
    printf("		<itemNumber>%d</itemNumber>\n",PositionsNr);
    printf("		<MSConsDestCode>%s</MSConsDestCode>\n",Bestimmungsland);
    printf("		<regionCode>%s</regionCode>\n",Ursprungsregion);
    printf("		<NatureOfTransaction>\n");
    printf("			<natureOfTransactionACode>%c</natureOfTransactionACode>\n",ArtDesGesch[0]);
    printf("			<natureOfTransactionBCode>%c</natureOfTransactionBCode>\n",ArtDesGesch[1]);
    printf("		</NatureOfTransaction>\n");
    printf("		<CN8>\n");
    printf("			<CN8Code>%s</CN8Code>\n",WTN);
    printf("		</CN8>\n");
    printf("		<netMass>%d</netMass>\n",Eigenmasse);
    printf("		<quantityInSU>%d</quantityInSU>\n",BesEinheit);
    printf("		<InvoicedAmount>%d</InvoicedAmount>\n",ReBetrag);
    printf("		<statisticalValue>%d</statisticalValue>\n",StatWert);
    printf("		<referencePeriod>20%s-%s</referencePeriod>\n",BezugsJahr,BezugsMonat);
    //printf("%d\n",);
    //printf("%d\n",);
    //printf("%d\n",);
    //printf("%d\n",);
    printf("	</item>\n");
   }
}
